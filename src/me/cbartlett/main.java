package me.cbartlett;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class main {
	
	private static List<Integer> dataList = new ArrayList<Integer>();
	
	private static int numberOfCaverns;
	
	private static List<Cavern> cavernList = new ArrayList<Cavern>();
	
	public static Map<Integer, Cavern> cavernMap = new HashMap<Integer, Cavern>();
	
	//private static File cavernFile = new File("H:\\Artificial Intelligence\\Coursework\\artificial-intelligence-cw\\caverns\\input1.cav");
	
	

	public static void main(String[] args) throws IOException {
		
		if(args.length < 1) {
			System.out.println("No arguments. At least 1 required.");
			return;
		}
		String arg = args[0].contains(".cav") ? args[0] : args[0]+".cav";
		File cavernFile = new File(System.getProperty("user.dir")+"\\"+arg);
	
		//File cavernFile = new File(args[0]);
		
		long startTime = System.currentTimeMillis();
		Scanner sc = new Scanner(cavernFile);

		
		String data = sc.next();
		sc.close();

		String[] tmpList = data.split(",");
		for(int i = 0; i<tmpList.length; i++) {
			dataList.add(Integer.parseInt(tmpList[i]));
		}
		

		
		if(dataList.size() > 0) {
			numberOfCaverns = dataList.get(0);
			
			int tmpId = 1;		
			for(int i = 1; i < numberOfCaverns*2; i=i+2) {
				Cavern c = new Cavern(dataList.get(i), dataList.get(i+1), tmpId);
				cavernList.add(c);
				tmpId++;
			}
			
			List<Integer> connectionData = new ArrayList<Integer>();		
					
			for(int i = numberOfCaverns*2+1; i<dataList.size(); i++) {
				connectionData.add(dataList.get(i));					
			}
			
			int tmpCavern = 0;
			int tmp = 0;
			for(int i = 0; i <connectionData.size(); i++) {
				Cavern c = cavernList.get(tmp);
				if(connectionData.get(i) == 1){
					cavernList.get(tmpCavern).getNeighbours().add(c);
				}
				tmpCavern++;
				if(tmpCavern >= numberOfCaverns) {
					tmpCavern = 0;
					tmp++;
				}							
			}

			// At this stage, the file has been parsed, caverns created & distance between each cavern that can connect has been calculated
			
			Cavern start = cavernList.get(0);
			Cavern goal = cavernList.get(cavernList.size() - 1);
			
			List<Cavern> closedSet = new ArrayList<Cavern>();
			List<Cavern> openSet = new ArrayList<Cavern>();
			
			Map<Cavern, Double> gScore = new HashMap<Cavern, Double>();
			Map<Cavern, Double> fScore = new HashMap<Cavern, Double>();
			
			gScore.put(start, 0.0D);
			fScore.put(start, start.getDistance(goal));
			
			Map<Cavern, Cavern> cameFrom  = new HashMap<Cavern, Cavern>();
			List<Cavern> path = new ArrayList<Cavern>();
			
			openSet.add(start);			
			
			while(!openSet.isEmpty()){
				Cavern current = null;
				for(Cavern c : openSet){
					if (current == null){
						current = c;
					} 
					if(fScore.get(c) < fScore.get(current)){
						current = c;
					}
				}
				if(current != null){
					openSet.remove(current);
					closedSet.add(current);
					
					// checking if reached goal
					if(current.equals(goal)){
				
						String total_path = current.getCavernId()+",";
						
						List<Cavern> route = new ArrayList<Cavern>();
						route.add(current);

						while(cameFrom.containsKey(current)){
							current = cameFrom.get(current);
							total_path+=current.getCavernId()+",";
							
							route.add(current);
						}
						//System.out.println(route);
						//System.out.println(total_path);
						
						/*double totalDistance = 0.0D;
						for(int i=0; i <route.size();i++){
							if(route.get(i).getCavernId() != 1){
								Cavern c = route.get(i);
								totalDistance+= c.getDistance(route.get(i+1));							}
						}
						System.out.println("####\n");
						Collections.reverse(route);
						System.out.println(route);
						System.out.println("Total distance: " + totalDistance);
						long endTime = System.currentTimeMillis();
						long total = endTime - startTime;
						System.out.println("Total calculation time: " + ((total)/1000)+"s - ("+total+" ms)");*/
						printResultToFile(route, args[0], true);
						return;
					}
					
					for(Cavern neighbour : current.getNeighbours()){
						if(closedSet.contains(neighbour)){
							continue;
						}
						
						double tmpGScore = gScore.get(current) + current.getDistance(neighbour);
						
						if(!openSet.contains(neighbour)){
							openSet.add(neighbour);
						} else if(tmpGScore >= gScore.get(neighbour)){
							continue;
						}
						path.add(neighbour);
						cameFrom.put(neighbour, current);
						gScore.put(neighbour, tmpGScore);   
						fScore.put(neighbour, tmpGScore + neighbour.getDistance(goal));
											
						}					
					}					
			}
			printResultToFile(null, args[0], false);
		}
	}
	
	public static void printResultToFile(List<Cavern> list, String filename, boolean success) throws IOException {
		filename = filename.contains(".cav") ? filename.substring(0, filename.length() - 4) : filename;
		try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename+".csn")))){
			if(success) {
				for(Cavern c : list) {
					writer.write(c+" ");
				}
			} else {
				writer.write("0");
			}
			
		}
	}
} 
