package me.cbartlett;

import java.util.ArrayList;
import java.util.List;

public class Cavern {
	
	private int xCoordinate;
	private int yCoordinate;
	private int cavernId;

	
	// Cavern ID, distance between this cavern and cavern it can connect to
	private List<Cavern> neighbours = new ArrayList<Cavern>();

	
	public Double getDistance(Cavern target){
		//Euclidean math here
		double x = Math.pow(target.getX() - this.getX(), 2);
		double y = Math.pow(target.getY() - this.getY(), 2);
		return Math.sqrt(x+y);
	}
	

	public List<Cavern> getNeighbours() {
		return this.neighbours;
	}

	public Cavern(int x, int y, int cavernId) {
		this.cavernId = cavernId;
		this.xCoordinate = x;
		this.yCoordinate = y;
		
	}
	
	public int getX() {
		return xCoordinate;
	}

	

	public int getY() {
		return yCoordinate;
	}

	
	@Override
	public String toString() {	
		
		/*if(connectivity.size() > 0 ) {
			String tmp =" ### \n";
			for(int i = 1; i <connectivity.size()+1; i++) {
				tmp+="Cavern "+cavernId+" connection to "+i+"? "+connectivity.get(i)+"\n";
				
			}
			
			return "Id "+this.getCavernId()+": "+this.getX() +", "+ this.getY() + tmp;
		}
			
		return "Id "+this.getCavernId()+": "+this.getX() +", "+ this.getY();*/
		return this.cavernId+"";
	}


	public int getCavernId() {
		return cavernId;
	}

}
